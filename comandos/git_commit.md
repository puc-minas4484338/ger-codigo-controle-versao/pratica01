# git commit
O comando git commit é usado para confirmar as alterações na cabeça. Tenha em atenção que quaisquer alterações efetuadas não irão para o repositório remoto. Uso:

# Exemplo
git commit –m “coloque sua mensagem aqui”